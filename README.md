# thcrap Plugin for Galaxy 2.0

This is a [GOG Galaxy 2.0](https://www.gogalaxy.com/) plugin intended to integrate [thcrap](https://github.com/thpatch/thcrap), a patching framework mainly used to translate and mod the Touhou Project games. The plugin's goal is allowing the Touhou games to be easily imported along with their translations/mods into Galaxy.

Due to limitations with Galaxy 2.0, only one global patch stack can be supported at any time, and until a proper platform ID is added, the plugin will show in Galaxy as "Testing purposes." This plugin is also intended for regular thcrap setups ([the main package](https://www.thpatch.net/wiki/Touhou_Patch_Center:Download)) and not for standalone setups.

### Installation

Copy the `thcrap_galaxy_plugin_0.1_2` folder to the following directory on Windows:

```%localappdata%\GOG.com\Galaxy\plugins\installed```

Then, enter the folder you copied and open `user_settings.json`, and edit the `thcrap_dir` value to include your thcrap folder, and edit the `default_patch_name` value so that it matches the name of the patch stack you want Galaxy to launch (that's the name you gave your patch when creating it through `thcrap_configure.exe`). For example, your `user_settings.json` file can look like this:

`"thcrap_dir": "C:\\thcrap",
"default_patch_name": "de"`

**BE SURE ALL BACKSLASHES ARE DOUBLED. If they aren't, the plugin will crash.**

Once finished, restart Galaxy, then click the cog at the top-left of Galaxy's window, click "Settings" then scroll down until you see "Testing purposes" and then click the "Connect" button right next to it and finally click "Confirm."

### Other notes

- You can find thcrap supported games in the "Testing purposes" view/bookmark. You can right click the bookmark and then click "Rename bookmark" to change the name.
- The integration username is "thcrap" so that you can identify it in the integrations settings later.
- Although you can launch any thcrap supported game, Galaxy can't track whether a game is running or not.
- If you remove a game from thcrap, it will still show as owned in Galaxy, but not installed.
- If your thcrap's `games.js` file includes Japanese characters, the plugin may crash. It may be necessary to rename TH06's executable, as it is the most likely culprit if not the game folders themselves.
- An "installer" may be made to ease the installation process in the future.