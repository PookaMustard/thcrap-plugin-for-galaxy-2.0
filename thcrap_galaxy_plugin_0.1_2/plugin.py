import asyncio
import sys
import re
import os
import subprocess
import json
from galaxy.api.plugin import Plugin, create_and_run_plugin
from galaxy.api.types import Game, LicenseInfo, LocalGame, Authentication, LocalGameState
from galaxy.api.errors import (AuthenticationRequired, UnknownBackendResponse,
                               AccessDenied, InvalidCredentials, UnknownError,
                               ApplicationError)
from galaxy.api.consts import Platform, LicenseType
from galaxy.api.jsonrpc import InvalidParams

SETTINGS_FILE = os.path.dirname(
    os.path.realpath(__file__)) + os.sep + "user_settings.json"
GAME_TITLES = os.path.dirname(
    os.path.realpath(__file__)) + os.sep + "game_titles.json"

TH12_FALLBACK = "TH_Seirensen"
TH13_FALLBACK = "TH13_Shinreibyou"
TH14_FALLBACK = "TH_Kishinjou"
TH155_FALLBACK = "TH_Hyouibana"


def load_settings():
    try:
        with open(SETTINGS_FILE, 'r') as settings_data:
            return json.load(settings_data)
    except:
        raise Exception("Settings file not found or unreadable.")


def load_game_title(game):
    try:
        with open(GAME_TITLES, 'r') as game_data:
            return json.load(game_data)[game]
    except:
        raise Exception("Game titles file not found or unreadable.")


class thcrap_plugin(Plugin):
    def __init__(self, reader, writer, token):
        super().__init__(Platform.Test, "0.1_2", reader, writer, token)
        self.local_games_cache = self.local_games()
        self.games = self.get_owned_games()

    async def authenticate(self, stored_credentials=None):
        return self.do_auth()

    async def pass_login_credentials(self, step, credentials, cookies):
        return self.do_auth()

    def do_auth(self):
        user_data = {}
        user_data["username"] = "thcrap"
        self.store_credentials(user_data)
        return Authentication("thcrap_user", user_data["username"])

    def local_games(self):
        local_games = []
        thcrap_dir = load_settings()['thcrap_dir'] + os.sep
        if os.path.isfile(thcrap_dir + "config" + os.sep + "games.js"):
            th_games = thcrap_dir + "config" + os.sep + "games.js"
        else:
            th_games = thcrap_dir + "games.js"
        with open(th_games, 'r') as th_games_js:
            found_games = json.load(th_games_js)

        for game in found_games:
            if game.lower().endswith("_custom"):
                continue
            else:
                if str(game) == "th12":
                    gameID = TH12_FALLBACK
                elif str(game) == "th13":
                    gameID = TH13_FALLBACK
                elif str(game) == "th14":
                    gameID = TH14_FALLBACK
                elif str(game) == "th155":
                    gameID = TH155_FALLBACK
                else:
                    gameID = str(game)
                local_games.append(
                    LocalGame(
                        gameID, 1
                    )
                )
        return local_games

    def get_state_changes(self, old_list, new_list):
        old_dict = {x.game_id: x.local_game_state for x in old_list}
        new_dict = {x.game_id: x.local_game_state for x in new_list}
        result = []
        # removed games
        result.extend(LocalGame(id, LocalGameState.None_)
                      for id in old_dict.keys() - new_dict.keys())
        # added games
        result.extend(
            local_game for local_game in new_list if local_game.game_id in new_dict.keys() - old_dict.keys())
        # state changed
        result.extend(LocalGame(id, new_dict[id]) for id in new_dict.keys(
        ) & old_dict.keys() if new_dict[id] != old_dict[id])
        return result

    async def get_owned_games(self):
        owned_games = []
        thcrap_dir = load_settings()['thcrap_dir'] + os.sep
        default_patch_name = load_settings()['default_patch_name']

        if os.path.isfile(thcrap_dir + "config" + os.sep + "games.js"):
            th_games = thcrap_dir + "config" + os.sep + "games.js"
        else:
            th_games = thcrap_dir + "games.js"

        with open(th_games, 'r') as th_games_js:
            found_games = json.load(th_games_js)

        for game in found_games:
            if game.lower().endswith("_custom"):
                continue
            else:
                if str(game) == "th12":
                    gameID = TH12_FALLBACK
                elif str(game) == "th13":
                    gameID = TH13_FALLBACK
                elif str(game) == "th14":
                    gameID = TH14_FALLBACK
                elif str(game) == "th155":
                    gameID = TH155_FALLBACK
                else:
                    gameID = str(game)
                owned_games.append(
                    Game(
                        gameID,
                        load_game_title(str(game)),
                        [],
                        LicenseInfo(LicenseType.SinglePurchase, None)
                    )
                )
        return owned_games

    async def get_local_games(self):
        return self.local_games_cache

    async def check_for_new_games(self):
        games = await self.get_local_games()
        for game in games:
            if game not in self.local_games_cache:
                self.local_games_cache.append(game)
        return

    async def launch_game(self, game_id):
        thcrap_dir = load_settings()['thcrap_dir']
        thcrap = thcrap_dir + os.sep + "thcrap_loader.exe"
        patch_name = load_settings()['default_patch_name']
        if not patch_name.lower().endswith(".js"):
            patch_name += ".js"
        if os.path.isfile(thcrap_dir + os.sep + patch_name):
            patch_name = thcrap_dir + os.sep + patch_name

        if game_id == TH12_FALLBACK:
            game_id = "th12"
        elif game_id == TH13_FALLBACK:
            game_id = "th13"
        elif game_id == TH14_FALLBACK:
            game_id = "th14"
        elif game_id == TH155_FALLBACK:
            game_id = "th155"

        return subprocess.Popen([thcrap, patch_name, game_id])
        # self.local_games_cache
        # p.wait()

    def tick(self):

        async def _update_local_games():
            loop = asyncio.get_running_loop()
            new_local_games_list = await loop.run_in_executor(
                                                None, self.local_games)
            notify_list = self.get_state_changes(
                self.local_games_cache, new_local_games_list)
            self.local_games_cache = new_local_games_list
            for local_game_notify in notify_list:
                self.update_local_game_status(local_game_notify)

        asyncio.create_task(_update_local_games())


def main():
    create_and_run_plugin(thcrap_plugin, sys.argv)


if __name__ == "__main__":
    main()
